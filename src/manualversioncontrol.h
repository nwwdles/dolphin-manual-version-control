/*****************************************************************************
 *   Copyright (C) 2014 by Emmanuel Pescosta <emmanuelpescosta099@gmail.com> *
 *   Copyright (C) 2012 by Sergei Stolyarov <sergei@regolit.com>             *
 *   Copyright (C) 2010 by Thomas Richard <thomas.richard@proan.be>          *
 *   Copyright (C) 2009-2010 by Peter Penz <peter.penz19@gmail.com>          *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA              *
 *****************************************************************************/

#ifndef ManualVersionControlPLUGIN_H
#define ManualVersionControlPLUGIN_H

#include <Dolphin/KVersionControlPlugin>

class ManualVersionControlPlugin : public KVersionControlPlugin
{
    Q_OBJECT

  public:
    ManualVersionControlPlugin(QObject *parent, const QVariantList &args);
    ~ManualVersionControlPlugin() override;

    QString fileName() const override;

    bool beginRetrieval(const QString &directory) override;
    KVersionControlPlugin::ItemVersion
    itemVersion(const KFileItem &item) const override;
    void endRetrieval() override;

    QList<QAction *> actions(const KFileItemList &items) const override;

  private slots:
    void handleContextAction(QAction *action);

  private:
    class Private;
    Private *const d;
    const QString m_dbFileName;
    QHash<QString, ItemVersion> m_fileMarksHash;

    QAction *m_UnversionedAction;
    QAction *m_NormalAction;
    QAction *m_UpdateRequiredAction;
    QAction *m_LocallyModifiedAction;
    QAction *m_AddedAction;
    QAction *m_RemovedAction;
    QAction *m_ConflictingAction;
    QAction *m_LocallyModifiedUnstagedAction;

    QAction *createAction(const QString &text,
                          KVersionControlPlugin::ItemVersion state,
                          QString iconName = "");
    void readFileMarks();
    void writeFileMarks();
};

#endif // ManualVersionControlPLUGIN_H
