/*****************************************************************************
 *   Copyright (C) 2014 by Emmanuel Pescosta <emmanuelpescosta099@gmail.com> *
 *   Copyright (C) 2012 by Sergei Stolyarov <sergei@regolit.com>             *
 *   Copyright (C) 2010 by Thomas Richard <thomas.richard@proan.be>          *
 *   Copyright (C) 2009-2010 by Peter Penz <peter.penz19@gmail.com>          *
 *                                                                           *
 *   This program is free software; you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation; either version 2 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the           *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program; if not, write to the                           *
 *   Free Software Foundation, Inc.,                                         *
 *   51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA              *
 *****************************************************************************/

#include "manualversioncontrol.h"

#include <KActionCollection>
#include <KActionMenu>
#include <KFileItem>
#include <KFileItemListProperties>
#include <KLocalizedString>
#include <KPluginFactory>

#include <QDebug>
#include <QDir>
#include <QFileSystemWatcher>
#include <QPointer>
#include <QSettings>
#include <QStandardPaths>
#include <QStringBuilder>
#include <QTextStream>

K_PLUGIN_FACTORY(ManualVersionControlPluginFactory,
                 registerPlugin<ManualVersionControlPlugin>();)

class ManualVersionControlPlugin::Private
{
  public:
    Private(ManualVersionControlPlugin *parent)
        : contextFilePaths(),
          databaseFileWatcher(new QFileSystemWatcher(parent)),
          contextActions(new KActionCollection(parent))
    {
    }

    QStringList contextFilePaths;
    QPointer<QFileSystemWatcher> databaseFileWatcher;
    QPointer<KActionCollection> contextActions;
};

ManualVersionControlPlugin::ManualVersionControlPlugin(QObject *parent,
                                                       const QVariantList &args)
    : KVersionControlPlugin(parent), d(new Private(this)),
      m_dbFileName(
          QDir(QStandardPaths::writableLocation(QStandardPaths::DataLocation))
              .path() %
          QDir::separator() % "manualversioncontrol")
{
    Q_UNUSED(args);

    KActionMenu *menuAction = new KActionMenu(d->contextActions);
    menuAction->setText("Manual Version Control");

    // qDebug() << m_dbFileName; // .local/share/dolphin/manualversioncontrol
    // clang-format off
    menuAction->addAction(createAction("Unversioned", UnversionedVersion));
    menuAction->addAction(createAction("Normal", NormalVersion, "vcs-normal"));
    menuAction->addAction(createAction("Added", AddedVersion, "vcs-added"));
    menuAction->addAction(createAction("Locally modified", LocallyModifiedVersion, "vcs-locally-modified"));
    menuAction->addAction(createAction("Locally modifed unstaged", LocallyModifiedUnstagedVersion, "vcs-locally-modified-unstaged"));
    menuAction->addAction(createAction("Update required", UpdateRequiredVersion, "vcs-update-required"));
    menuAction->addAction(createAction("Conflicting", ConflictingVersion, "vcs-conflicting"));
    menuAction->addAction(createAction("Removed", RemovedVersion, "vcs-removed"));
    // clang-format on
    // menuAction->setIcon(QIcon::fromTheme("favorites"));

    d->contextActions->addAction("a", menuAction);

    readFileMarks();
}

ManualVersionControlPlugin::~ManualVersionControlPlugin() { delete d; }

QString ManualVersionControlPlugin::fileName() const { return QString(); }

bool ManualVersionControlPlugin::beginRetrieval(const QString &directory)
{
    Q_ASSERT(directory.endsWith(QLatin1Char('/')));

    auto dir = QDir(directory);
    do {
        auto git_marker = dir.exists(".git");
        auto dropbox_marker = dir.exists(".dropbox.cache");
        if (git_marker || dropbox_marker) {
            return false;
        }
    } while (dir.cdUp());

    return true;
}

KVersionControlPlugin::ItemVersion
ManualVersionControlPlugin::itemVersion(const KFileItem &item) const
{
    const QString itemUrl = item.localPath();
    if (m_fileMarksHash.contains(itemUrl)) {
        return m_fileMarksHash.value(itemUrl);
    } else {
        // files that are not in our map are normal, tracked files by definition
        return UnversionedVersion;
    }
}

void ManualVersionControlPlugin::endRetrieval() {}

QList<QAction *>
ManualVersionControlPlugin::actions(const KFileItemList &items) const
{
    Q_ASSERT(!items.isEmpty());

    d->contextFilePaths.clear();

    foreach (const KFileItem &item, items) {
        d->contextFilePaths << QDir(item.localPath()).canonicalPath();
    }

    return d->contextActions->actions();
}

void ManualVersionControlPlugin::handleContextAction(QAction *action)
{
    bool updated = false;
    foreach (const QString &fileUrl, d->contextFilePaths) {
        auto state = KVersionControlPlugin::ItemVersion(action->data().toInt());
        if (state != static_cast<int>(UnversionedVersion)) {
            m_fileMarksHash.insert(fileUrl, state);
        } else {
            m_fileMarksHash.remove(fileUrl);
        }
        updated = true; // this is probably unneeded
    }
    if (updated) {
        emit itemVersionsChanged();
        writeFileMarks();
    }
}

QAction *ManualVersionControlPlugin::createAction(
    const QString &text, KVersionControlPlugin::ItemVersion state,
    QString iconName)
{
    auto newAction = new QAction(this);
    newAction->setData(state);
    newAction->setText(xi18nd("@action:inmenu", text.toStdString().c_str()));
    connect(newAction, &QAction::triggered, this,
            [this, newAction]() { this->handleContextAction(newAction); });

    if (iconName != "")
        newAction->setIcon(QIcon::fromTheme(iconName));

    return newAction;
}

void ManualVersionControlPlugin::readFileMarks()
{
    QSettings settings(m_dbFileName, QSettings::NativeFormat, this);
    settings.beginGroup("marks");
    for (auto i = 0; i < 8; ++i) {
        int size = settings.beginReadArray(QString(i));
        for (int j = 0; j < size; ++j) {
            settings.setArrayIndex(j);
            auto url = settings.value("url").toString();
            m_fileMarksHash.insert(
                url, static_cast<KVersionControlPlugin::ItemVersion>(i));
        }
        settings.endArray();
    }
    settings.endGroup();
}

void ManualVersionControlPlugin::writeFileMarks()
{
    const int size = 8;
    QVector<QList<QString>> lists(size);
    for (auto it = m_fileMarksHash.cbegin(), end = m_fileMarksHash.cend();
         it != end; ++it) {
        lists[it.value()].append(it.key());
    }
    QSettings settings(m_dbFileName, QSettings::NativeFormat, this);
    settings.clear();
    settings.beginGroup("marks");
    for (auto i = 0; i < size; ++i) {
        settings.beginWriteArray(QString(i));
        auto &fileList = lists[i];
        for (int j = 0; j < fileList.size(); ++j) {
            settings.setArrayIndex(j);
            settings.setValue("url", fileList.at(j));
        }
        settings.endArray();
    }
    settings.endGroup();
}

#include "manualversioncontrol.moc"