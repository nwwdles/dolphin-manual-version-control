# dolphin-manual-version-control

---

**NOTE:**
it works but it will conflict with all other dolphin plugins (git, dropbox etc.). It may be fixed if I use marker files/dirs (like `.git` or `.dropbox` etc.) instead of doing things the current way.

---

Manual mode version control plugin for Dolphin. Mark files and add emblems to them manually.

It keeps a list of marked files in `~/.local/share/dolphin/manualversioncontrol`.

## Installation

Arch-based: use `PKGBUILD`, otherwise install path may be wrong (or you can use the commands from inside the `PKGBUILD` obviously):

```sh
curl https://gitlab.com/cupnoodles14/dolphin-manual-version-control/raw/master/PKGBUILD > PKGBUILD
makepkg --install
```

Everyone else should probably be okay with:

```sh
git clone https://gitlab.com/cupnoodles14/dolphin-manual-version-control
cd dolphin-manual-version-control
mkdir build && cd build
cmake ..
sudo make install
```

## Screenshots

Marked files:

![Marked files](https://i.imgur.com/IFOTQrk.png)

Service menu:

![Service menu](https://i.imgur.com/pXR4Ub5.png)

---

## Todo

- [ ] An option to remove all unreachable files from the db.
- [ ] Preview highlight colors in the context menu.
- [ ] Fix the code to use marker files
  - [ ] Store database entries in marker files
